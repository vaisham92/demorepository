/**
 * New node file
 */
var request = require('request')
, express = require('express')
,assert = require("assert")
,http = require("http");

describe('http tests', function(){

	it('should return the calculator home page if the url is correct', function(done){
		http.get('http://localhost:3000/', function(res) {
			assert.equal(200, res.statusCode);
			done();
		})
	});

	it('should return the calculated value correctly', function(done){
		request({
				url: 'http://localhost:3000/calculator',
				method: "POST",
				headers: {
					"content-type": "application/json",
					},
				json: {"operandOne":"9","operandTwo":"18","operation":"Addition"}
			}, function (error, response, body) { 
				assert.equal(27, body);
				done();
			});
	});
});